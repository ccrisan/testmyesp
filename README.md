**testMyESP** is a tool to run automated tests for the ESP8266.

For more details check out the project's [wiki](https://gitlab.com/ccrisan/testmyesp/wikis/home).